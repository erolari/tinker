var colors = require('colors');
const peoples = require('./people');
const tr = require('./trinker');
const people = tr.transforme_salaire(peoples)

console.log(tr.title());
console.log("Model des données : ");
console.log(people[0]);
console.log(tr.line('LEVEL 1'));
console.log("Nombre d'hommes : ",                                                                   tr.allMale(people).length);
console.log("Nombre de femmes : ",                                                                  tr.allFemale(people).length);
console.log("Nombre de personnes qui cherchent un homme :",                                         tr.nbOfMaleInterest(people).length);
console.log("Nombre de personnes qui cherchent une femme :",                                        tr.nbOfFemaleInterest(people).length);
console.log("Nombre de personnes qui gagnent plus de 2000$ :",                                      tr.compare_salaire(people,2000,"+").length);
console.log("Nombre de personnes qui aiment les Drama :",                                           tr.preffilm(people,"Drama").length);
console.log("Nombre de femmes qui aiment la science-fiction :",                                     tr.preffilm(tr.allFemale(people),"Sci-Fi").length);
console.log(tr.line('LEVEL 2'));
console.log("Nombre de personnes qui aiment les documentaires et gagnent plus de 1482$ :",            tr.preffilm(tr.compare_salaire(people,1482,"+"),"Documentary").length);
console.log("Liste des noms, prénoms, id et revenu des personnes qui gagnent plus de 4000$ :",      tr.retournedonnée(tr.compare_salaire(people,4000,"+"),["id","last_name","first_name","income"]));
console.log("Homme le plus riche (nom et id) :",                                                    tr.retournedonnée(tr.trie(tr.allMale(people),"croissant"),["id",'last_name'])[0]);
console.log("Salaire moyen :",                                                                      tr.moyenne(people,"income"));
console.log("Salaire médian :",                                                                     tr.medianne(people,"income"));
console.log("Nombre de personnes qui habitent dans l'hémisphère nord :",                            tr.habitant_hemisphere(people, "nord").length);
console.log("Salaire moyen des personnes qui habitent dans l'hémisphère sud :",                     tr.moyenne(tr.habitant_hemisphere(people, "sud"),"income"));
console.log(tr.line('LEVEL 3'));            
console.log("Personne qui habite le plus près de Bérénice Cawt (nom et id) :",                      tr.retournedonnée(tr.triepardistavec(tr.retrouvepersonne(people,"Bérénice" ,"Cawt"),people)[0],["id",'last_name', "dist"]));
console.log("Personne qui habite le plus près de Ruì Brach (nom et id) :",                          tr.retournedonnée(tr.triepardistavec(tr.retrouvepersonne(people,"Brach" ,"Ruì"),people)[0],["id",'last_name', "dist"]));
console.log("les 10 personnes qui habite les plus près de Josée Boshard (nom et id) :",             tr.retournedonnée(tr.triepardistavec(tr.retrouvepersonne(people,"Josée" ,"Boshard"),people).slice(0,10),["id",'last_name', "dist"]));
console.log("Les noms et ids des 23 personnes qui travaillent chez google :",                       tr.retournedonnée(people.filter(p => p.email.includes('google')),["id",'last_name']));
console.log("Personne la plus agée :",                                                              tr.age(people)[tr.age(people).length - 1]);
console.log("Personne la plus jeune :",                                                             tr.age(people)[0]);
console.log("Moyenne des différences d'age :",                                                      tr.differencemoyenneage(people));
console.log(tr.line('LEVEL 4'));            
console.log("Genre de film le plus populaire :",                                                    tr.goutDesGensFilm(people)[0]);
console.log("Genres de film par ordre de popularité :",                                             tr.goutDesGensFilm(people));
console.log("Liste des genres de film et nombre de personnes qui les préfèrent :",                  tr.goutDesGensFilm(people, "complet"));
console.log("Age moyen des hommes qui aiment les films noirs :",                                    tr.moyenne(tr.age(tr.preffilm(tr.allMale(people), "Film-Noir")), "age"));
console.log(`Age moyen des femmes qui aiment les drames et habitent sur le fuseau horaire 
de Paris :`,                                                                                        tr.moyenne(tr.preffilm(tr.habitantfuseauhoraire(tr.allFemale(people),0,15),"Drama"),"age"));
console.log(`Homme qui cherche un homme et habite le plus proche d'un homme qui a au moins une 
 préférence de film en commun (afficher les deux et la distance entre les deux):`,                  tr.lesdeuxplusproche(tr.lesplusprocheparcategoriefilm((tr.nbOfMaleInterest(tr.allMale(people))))));
console.log("Liste des couples femmes / hommes qui ont les même préférences de films :",            tr.lesplusprocheparcategoriefilmhetero(tr.memepreferencefilm2groupedepersonne(tr.hetero(people))));
console.log(tr.line('MATCH'));
/* 
    On match les gens avec ce qu'ils cherchent (homme ou femme).
    On prend en priorité ceux qui sont les plus proches.
    Puis ceux qui ont le plus de goût en commun.
    Pour les couples hétéroséxuel on s'assure que la femme gagne au moins 10% de moins que l'homme mais plus de la moitié. 
    (C'est comme ça que fonctionne les sites de rencontre ! https://editionsgouttedor.com/catalogue/lamour-sous-algorithme/)
    Les gens qui travaillent chez google ne peuvent qu'être en couple entre eux.
    Quelqu'un qui n'aime pas les Drama ne peux pas être en couple avec quelqu'un qui les aime.
    Quelqu'un qui aime les films d'aventure doit forcement être en couple avec quelqu'un qui aime aussi les films d'aventure.
    Le différences d'age dans un couple doit être inférieure à 25% (de l'age du plus agée des deux)
    ߷    ߷    ߷    Créer le plus de couples possibles.   ߷    ߷    ߷    
    ߷    ߷    ߷    Mesurez le temps de calcul de votre fonction   ߷    ߷    ߷    
    ߷    ߷    ߷    Essayez de réduire le temps de calcul au maximum   ߷    ߷    ߷    
*/
console.log("liste de couples à matcher (nom et id pour chaque membre du couple) :",             tr.match(people));