const people = require("./people");
const { forEach } = require("./people");

module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    allMale: function(p){
        return p.filter(p => p["gender"] == "Male");
    },

    allFemale: function(p){
        return (p.filter(p => !(p["gender"] == "Male")));
    },

    nbOfMaleInterest: function(p){
        return p.filter(p => p["looking_for"] == "M")
        
    },

    nbOfFemaleInterest: function(p){
        return p.filter(p => p["looking_for"] == "F")
    },
    compare_salaire: function(p,salaire,comparateur){
        if(comparateur == "plus" || comparateur == "+"){
            return p.filter(p => p["income"] > salaire)

        }
        else if(comparateur == "-" || comparateur == "moins"){
            return p.filter(p => p["income"] < salaire)
        }
        else{
            return p.filter(p => p["income"] == salaire)
        }
    },


    preffilm: function(p,pref){
        return p.filter(p => p.pref_movie.includes(pref))
    },
    retournedonnée: function(p,elems){
        tableau_des_resultats = []
        tabtransitoire = []
        if(p.length == undefined){
            elems.forEach(elem =>
                tabtransitoire.push(p[elem])
                )
                return tabtransitoire
        }
        p.forEach(
            personne => {
                elems.forEach(elem =>
                    tabtransitoire.push(personne[elem])
                    )
                    tableau_des_resultats.push(tabtransitoire)
                    tabtransitoire =[]
                }
        )
        return tableau_des_resultats
    },
    trie: function(p, sens){
        if(sens == "+" || sens == "croissant" || sens == "monte"){
            return p.sort(function (b, a) {
                return a["income"] - b["income"];
            });
        }
        return p.sort(function (b, a) {
            return b["income"] - a["income"];
        });
    
    },
    transforme_salaire: function(p){
        p.forEach(elem => elem.income = parseFloat(elem["income"].slice(1)))
        return p

    },
    moyenne: function(p,comparateur){
        let moyenne = 0
        p.forEach(elem => moyenne += elem[comparateur])
        return moyenne/p.length
    },
    medianne: function(p,comparateur){
        elem = this.trie(p , "+")
        mid = p.length/2
        if(p.length % 2 == 0){
            return (elem[mid][comparateur] + elem[mid-1][comparateur])/2
        }
        return elem[mid+0,5][comparateur]
    },
    habitant_hemisphere: function(p,comparateur){
        if(comparateur == 'nord' || comparateur == 'Nord'){
            return p.filter(p => p.latitude > 0)

        }
        return p.filter(p => p.latitude < 0)
    },
    retrouvepersonne: function(p,nom, prenom){
        d = p.filter(p => p.last_name == nom)
        d.filter(p => p.first_name == prenom)   
        if(d[0] == undefined ){
            p = p.filter(p => p.last_name == prenom)
            return p.filter(p => p.first_name == nom)
        }
        return d
    },
    // calcule distance terre arrondie du au fait que c'est pas une sphere constante https://geodesie.ign.fr/contenu/fichiers/Distance_longitude_latitude.pdf
    // latitudes φA et φB et de longitudes λA et λB 
    // dλ = λ B – λ A 
    // S A-B = arc cos (sin φ A sin φB + cos φ A cos φ B cos dλ)
    // la distance S en mètres, s’obtient en multipliant S A-B par un rayon de la Terre conventionnel (6 378 137 mètres par exemple)
    calculedist: function(p1,p2,option){
        if(option != "objetseul"){
            p1 = p1[0]
        }
        d = p1.longitude - p2.longitude
        lat1 = p1.latitude
        lat2 = p2.latitude
        s = (Math.cos(lat1) * Math.cos(lat2) * Math.cos(d) + Math.sin(lat1) * Math.sin(lat2))
        dist = s * 6378137
        return dist
    },

    triepardistavec: function(personne,p){
        p.forEach(element => { 
            dist = this.calculedist(personne,element)
            if(dist < 0){
                dist = dist * -1
            }
            element.dist = dist
        })
        return p.sort(function(a, b) {
            return a.dist - b.dist;
          });
    },
    age: function(p){
        p.forEach(element => { 
            date  = Date.now() - new Date(element.date_of_birth).getTime()
            age = date / 31557600000
            element.age = age
        })
        return p.sort(function(a, b) {
            return a.age - b.age;
          });
    },
    differencemoyenneage: function(p){
        p = this.age(p)
        let moyenne = 0
        p.forEach(personneAcomparer => {
            p.forEach(comparaisonAvecLesAutres =>{
                if(personneAcomparer.id != comparaisonAvecLesAutres.id){
                    dif = personneAcomparer.age - comparaisonAvecLesAutres.age
                    if(dif < 0){
                       dif = dif * -1
                    }
                    moyenne += dif
                }
            })
            personneAcomparer.moyenne = moyenne/p.length 
            moyenne = 0
        })
        return this.moyenne(p,"moyenne")
    },
    goutDesGensFilm: function(p,option){
        compt = {}
        p.forEach(personne =>{ 
            split = personne.pref_movie.split("|")
            split.forEach(film => {
             if(compt[film]){
                compt[film] += 1
             }
             else{
                compt[film] = 1
             }
            })
        }
            )
            tabdesfilm = Object.entries(compt).sort(([,a],[,b]) => b-a)
            if(option ==="complet"){
                return tabdesfilm
            }
            else{
                tabrognerdesfilms = []
                tabdesfilm.forEach(film => 
                    tabrognerdesfilms.push(film[0]))
                return tabrognerdesfilms

            }
    },
    // il suffit de connaître son décalage horaire avec le méridien choisi comme origine (aujourd'hui, celui de Greenwich). 1 heure de décalage correspond à 360/24=15 degrés de longitude
    //  donc entre 0 et 15 on et sur le meme fuseau horaire que paris 
    habitantfuseauhoraire(p,min,max){
        gens = p.filter(p => p.longitude > min)
        gens = gens.filter(p => p.longitude < max)
        return gens 
    },
    memepreferencefilm: function(p){
        lesgoutdugroupes = this.goutDesGensFilm(p)
        classepargout = []
        lesgoutdugroupes.forEach(gout => classepargout[gout] = [])
        p.forEach(personne => {
            let split = personne.pref_movie.split('|')
            split.forEach(split => classepargout[split].push(personne))
        })
        return classepargout
    },
    lesplusprocheparcategoriefilm: function(p,multiple){
        // nessecite  memepreferencefilm
        p = this.memepreferencefilm(p)
        if(multiple != "multiple"){
            resultat2 = {}
        }
        lesgoutdugroupes.forEach(categoriefilm => {
            if(p[categoriefilm].length > 1){
                resultat2[categoriefilm] = []
                for (let i = 0; i < p[categoriefilm].length; i++) {
                    for (let y = i+1; y < p[categoriefilm].length; y++) {
                        ecart_age = 0
                        personne1 = p[categoriefilm][i]
                        personne2 = p[categoriefilm][y]
                        age1 = personne1.age
                        age2 = personne2.age
                        if(multiple == "multiple"){
                            if(age1 > age2){
                                ecart_age = age1*0.75 < age2
                                
                            }
                            else{
                                ecart_age = age2*0.75 < age1
                            }
                            if(ecart_age){
                                let dist = this.calculedist(personne1,personne2,"objetseul")
                                    if(dist < 0){
                                        dist = dist * -1
                                    }
                                    if(personne1 && personne2){
                                        resultat2[categoriefilm].push([personne1, personne2,dist])
                                    }
                            }
                        }
                        else{
                            
                            let dist = this.calculedist(personne1,personne2,"objetseul")
                                if(dist < 0){
                                    dist = dist * -1
                                }
                                if(personne1 && personne2){
                                    resultat2[categoriefilm].push([personne1, personne2,dist])
                                }
                        }
                    }   
                }
        }
        // sans le array.from cela ne marche pas ,pas d'explication a chercher
        resultat2[categoriefilm] = Array.from(new Set(resultat2[categoriefilm]))
        resultat2[categoriefilm] = resultat2[categoriefilm].sort(function (a, b) { return a[2] - b[2]});
        } )
        return resultat2
    },
    lesdeuxplusproche: function(p){
        // nessecite la fonction lesplusprocheparcategoriefilm
        min = p['Drama'][0][2]
        let resultat = p['Drama'][0]
        lesgoutdugroupes.forEach(categoriefilm => {   
                if(p[categoriefilm][0] && p[categoriefilm][0][2] < min ){
                    min = p[categoriefilm][0][2]
                    resultat = p[categoriefilm][0]
                }
            
    })
        return resultat
    },
    hetero: function(p){
        let hetero = []
        hetero.push(this.nbOfMaleInterest(this.allFemale(p)))
        hetero.push(this.nbOfFemaleInterest(this.allMale(p)))
        return hetero
        
    },
    memepreferencefilm2groupedepersonne: function(p){
        lesgoutdugroupes = this.goutDesGensFilm(people)
        classepargout = []
        lesgoutdugroupes.forEach(gout => classepargout[gout] = [])
        p[0].forEach(personne => {
            let split = personne.pref_movie.split('|')
            split.forEach(split => classepargout[split].push(personne))
        })
        p[1].forEach(personne => {
            let split = personne.pref_movie.split('|')
            split.forEach(split => classepargout[split].push(personne))
        })
        return classepargout
    },
    lesplusprocheparcategoriefilmhetero: function(p){
        // nessecite  memepreferencefilm
        let resultat = {}
        lesgoutdugroupes.forEach(categoriefilm => {
            if(p[categoriefilm].length > 1){
                resultat[categoriefilm] = []
                for (let i = 0; i < p[categoriefilm].length; i++) {
                    for (let y = i+1; y < p[categoriefilm].length; y++) {
                        personne1 = p[categoriefilm][i]
                        personne2 = p[categoriefilm][y]
                        if(personne1.gender === "Male" && personne2.gender === "Female" || personne2.gender === "Male" && personne1.gender === "Female"){

                            let dist = this.calculedist(personne1,personne2,"objetseul")
                            if(dist < 0){
                                dist = dist * -1
                            }
                            if(personne1 && personne2){
                                resultat[categoriefilm].push([personne1, personne2,dist])
                            }
                        }
                        }   
                    }
                }
                // sans le array.from cela ne marche pas ,pas d'explication a chercher
        resultat[categoriefilm] = Array.from(new Set(resultat[categoriefilm]))
        resultat[categoriefilm] = resultat[categoriefilm].sort(function (a, b) { return a[2] - b[2]});
        } )
        return resultat
    },
    lesplusprocheparcategoriefilmheterobis: function(p){
        p = this.memepreferencefilm2groupedepersonne(p)
        // nessecite  memepreferencefilm
        lesgoutdugroupes.forEach(categoriefilm => {
            if(p[categoriefilm].length > 1){
                resultat[categoriefilm] = []
                for (let i = 0; i < p[categoriefilm].length; i++) {
                    for (let y = i+1; y < p[categoriefilm].length; y++) {
                        personne1 = p[categoriefilm][i]
                        personne2 = p[categoriefilm][y]
                        age1 = p[categoriefilm][y].age
                        age2 = p[categoriefilm][i].age
                        salaire1 = p[categoriefilm][i].income
                        salaire2 = p[categoriefilm][y].income
                        ecart_age = 0
                        ecart_salaire = 0
                        if(age1 > age2){
                            ecart_age = age1*0.75 < age2
                            
                        }
                        else{
                            ecart_age = age2*0.75 < age1
                        }
                        if(salaire1 > salaire2){
                            if(p[categoriefilm][i].gender == "Male"){
                                ecart_salaire = (salaire1*0.90 > salaire2 && salaire1*0.50 < salaire2)
                            }
                        }
                        else{
                            if(p[categoriefilm][y].gender == "Male"){
                                ecart_salaire = (salaire2*0.90 > salaire1 && salaire2*0.50 < salaire1)
                            }
                        }
                        if(ecart_age && ecart_salaire){
                        if(personne1.gender === "Male" && personne2.gender === "Female" || personne2.gender === "Male" && personne1.gender === "Female"){

                            let dist = this.calculedist(personne1,personne2,"objetseul")
                            if(dist < 0){
                                dist = dist * -1
                            }
                            if(personne1 && personne2){
                                resultat[categoriefilm].push([personne1, personne2,dist])
                            }
                        }
                    }
                }
            }   
                // sans le array.from cela ne marche pas ,pas d'explication a chercher
                resultat[categoriefilm] = Array.from(new Set(resultat[categoriefilm]))
                resultat[categoriefilm] = resultat[categoriefilm].sort(function (a, b) { return a[2] - b[2]});
            }}) 
        return resultat
    },
    match: function(p){
        resultat = {}
        resultat2 = {}
        match = []
        lesDramatiques = p.filter(p => p.pref_movie.includes("Drama") && !p.email.includes('google'))
        lesAventuriers = p.filter(p => p.pref_movie.includes("Adventure") && !p.email.includes('google'))
        lesGoogleist = p.filter(p => p.email.includes('google'))
        lesAutres = p.filter(p => !p.pref_movie.includes("Drama") && !p.pref_movie.includes("Adventure") && !p.email.includes('google'))
        tableau_des_preference = [lesAventuriers,lesAutres, lesGoogleist,lesDramatiques]
        tableau_des_preference.forEach(personne =>{
            les_HomosexuelM = this.nbOfMaleInterest(this.allMale(personne))
            les_HomosexuelF = this.nbOfFemaleInterest(this.allFemale(personne))
            les_heteros = this.hetero(personne)
            match_hetero = this.lesplusprocheparcategoriefilmheterobis(les_heteros)
            match_homo = this.lesplusprocheparcategoriefilm(les_HomosexuelM, "multiple")
            match_homo = this.lesplusprocheparcategoriefilm(les_HomosexuelF, "multiple")
            
        
        
     
        }
            )
        compt = 0
        for (const [key, value] of Object.entries(match_homo)) {
            value.forEach(tab => {
               match.push([tab[0].last_name, tab[0].id ,tab[1].last_name, tab[1].id])
            })
          }
          for (const [key, value] of Object.entries(match_hetero)) {
            value.forEach(tab => {
                match.push([tab[0].last_name, tab[0].id ,tab[1].last_name, tab[1].id])
             })
          }
        return [match, match.length + " match"]
    }
}
